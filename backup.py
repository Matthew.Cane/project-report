from distutils.dir_util import copy_tree
import os, subprocess

destinationDir = "C:\\Users\\mattc\\Documents\\Git\\project-report"
sourceDir = os.getcwd()
print("[1]\t INITIATING AUTO BACKUP")
print("[2]\t ATTEMPTING DIRECTORY CLONE")
copy_tree(sourceDir, destinationDir)
print("[3]\t DIRECTORY CLONE COMPLETE")
print("[4]\t ATTEMPTING GIT COMMIT AND PUSH")
call = "cd "+destinationDir+"&& git add . && git commit -m \"Auto Backup from Google Drive\" && git push"
subprocess.call(call, shell=True)
print("[5]\t GIT COMMIT AND PUSH COMPLETE")
input()
