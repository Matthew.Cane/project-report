import time, hmac, hashlib, sys

def getTimestamp():
    return int(round(time.time(), -1))

def performHMAC(key, message):
    h = hmac.new(bytes(key, 'ascii'), str(message).encode('ascii'), hashlib.sha256)
    return h.hexdigest()

def client(secret, UUID):
    hash = performHMAC(secret, getTimestamp())
    QRString = UUID + ":" + hash
    door(QRString)

def door(code):
    response = server(code)
    if response == True:
        print("Door Unlocked")
        exit()
    print("Door Locked")
    exit()

def server(encodedMessage):
    time.sleep(5)
    userList = {
        "MattCane":"PASSWORD",
        "Zaff":"Bella"
    }

    splitMessage = encodedMessage.split(':')

    if splitMessage[0] in userList:
        userPass = userList[splitMessage[0]]
    else:
        return False

    hash = performHMAC(userPass, getTimestamp())
    if hash == splitMessage[1]:
        return True
    return False


UUID = sys.argv[1]
secret = sys.argv[2]

client(secret, UUID)
