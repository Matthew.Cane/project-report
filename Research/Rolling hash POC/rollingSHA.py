import hashlib, secrets

def hashString(string):
    return hashlib.sha256(string.encode('utf-8')).hexdigest()

origin = secrets.token_hex(32)
hash1, hash2, hash3 = "", "", ""
hash1 = origin

for i in range (1, 10):
    stringToHash = hash1+hash2+hash3
    newHash = hashString(str(int(stringToHash, 16)))
    hash3 = hash2
    hash2 = hash1
    hash1 = newHash
    print("Code Number "+str(i)+": "+str(int(newHash, 16))+"\n")
