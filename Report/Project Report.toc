\contentsline {section}{\numberline {1}Introduction}{1}{section.1}%
\contentsline {section}{\numberline {2}Research}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Message Authentication Code (MAC)}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Hash-Based Message Authentication Code (HMAC)}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}HMAC Based One-Time Password (HOTP)}{4}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Dynamic Truncation}{5}{subsubsection.2.3.1}%
\contentsline {subsection}{\numberline {2.4}Time-Based One-Time Passwords (TOTP)}{6}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Matrix Barcodes}{6}{subsection.2.5}%
\contentsline {subsubsection}{\numberline {2.5.1}Quick Response (QR)}{7}{subsubsection.2.5.1}%
\contentsline {subsubsection}{\numberline {2.5.2}Aztec}{8}{subsubsection.2.5.2}%
\contentsline {subsubsection}{\numberline {2.5.3}Data Matrix}{10}{subsubsection.2.5.3}%
\contentsline {subsubsection}{\numberline {2.5.4}PDF417}{10}{subsubsection.2.5.4}%
\contentsline {subsubsection}{\numberline {2.5.5}Images Within Matrix Barcodes}{11}{subsubsection.2.5.5}%
\contentsline {subsubsection}{\numberline {2.5.6}Best Suited Matrix Barcode}{12}{subsubsection.2.5.6}%
\contentsline {subsection}{\numberline {2.6}Software Libraries}{12}{subsection.2.6}%
\contentsline {subsubsection}{\numberline {2.6.1}PiCamera}{13}{subsubsection.2.6.1}%
\contentsline {subsubsection}{\numberline {2.6.2}Pillow}{13}{subsubsection.2.6.2}%
\contentsline {subsubsection}{\numberline {2.6.3}PyZBar}{14}{subsubsection.2.6.3}%
\contentsline {subsection}{\numberline {2.7}Docker and Containerisation}{14}{subsection.2.7}%
\contentsline {subsection}{\numberline {2.8}Dashboard Frameworks}{16}{subsection.2.8}%
\contentsline {subsubsection}{\numberline {2.8.1}Tipboard}{16}{subsubsection.2.8.1}%
\contentsline {subsubsection}{\numberline {2.8.2}Grafana}{17}{subsubsection.2.8.2}%
\contentsline {subsection}{\numberline {2.9}Database Solutions}{17}{subsection.2.9}%
\contentsline {subsubsection}{\numberline {2.9.1}Relational Vs Non-Relational Database models}{17}{subsubsection.2.9.1}%
\contentsline {subsubsection}{\numberline {2.9.2}Client-Server Vs Embedded Databases}{18}{subsubsection.2.9.2}%
\contentsline {section}{\numberline {3}Requirements}{20}{section.3}%
\contentsline {subsection}{\numberline {3.1}Stakeholders}{20}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Functional Requirements}{21}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}MUST Requirements}{21}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}SHOULD Requirements}{22}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}COULD Requirements}{22}{subsubsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}WON'T Requirements}{22}{subsubsection.3.2.4}%
\contentsline {subsection}{\numberline {3.3}Non-Functional Requirements}{22}{subsection.3.3}%
\contentsline {section}{\numberline {4}Design}{24}{section.4}%
\contentsline {subsection}{\numberline {4.1}System Overview}{24}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Android Application}{26}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}User Interface}{26}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Security Considerations}{28}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Transferring User Secrets}{29}{subsubsection.4.2.3}%
\contentsline {subsection}{\numberline {4.3}Door Scanner}{30}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Cost}{32}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Flexibility}{32}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Form Factor}{33}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Software Compatibility}{33}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Library Performance on the Raspberry Pi Zero W}{33}{subsubsection.4.3.5}%
\contentsline {subsection}{\numberline {4.4}Back-End}{35}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}API}{35}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Web Interface}{36}{subsubsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.3}Database}{37}{subsubsection.4.4.3}%
\contentsline {section}{\numberline {5}Implementation}{42}{section.5}%
\contentsline {subsection}{\numberline {5.1}Android Application}{42}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Door Scanner}{43}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Software}{44}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Hardware}{46}{subsubsection.5.2.2}%
\contentsline {subsection}{\numberline {5.3}Back-End}{50}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}API}{53}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Web Interface}{54}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}Database}{55}{subsubsection.5.3.3}%
\contentsline {section}{\numberline {6}Conclusion}{60}{section.6}%
\contentsline {section}{\numberline {7}Glossary}{63}{section.7}%
\contentsline {paragraph}{Activity}{63}{section*.42}%
\contentsline {paragraph}{API}{63}{section*.43}%
\contentsline {paragraph}{CORS}{63}{section*.44}%
\contentsline {paragraph}{DBMS}{63}{section*.45}%
\contentsline {paragraph}{ERD}{63}{section*.46}%
\contentsline {paragraph}{Fail-Safe}{63}{section*.47}%
\contentsline {paragraph}{Fail-Secure}{63}{section*.48}%
\contentsline {paragraph}{FoV}{63}{section*.49}%
\contentsline {paragraph}{GPIO}{63}{section*.50}%
\contentsline {paragraph}{IDE}{63}{section*.51}%
\contentsline {paragraph}{LRD}{63}{section*.52}%
\contentsline {paragraph}{PCB}{63}{section*.53}%
\contentsline {paragraph}{Pseudocode}{63}{section*.54}%
\contentsline {paragraph}{PWA}{63}{section*.55}%
\contentsline {paragraph}{REST}{63}{section*.56}%
\contentsline {paragraph}{RFID}{63}{section*.57}%
\contentsline {paragraph}{SHA}{63}{section*.58}%
\contentsline {paragraph}{SSL}{64}{section*.59}%
\contentsline {paragraph}{TLS}{64}{section*.60}%
\contentsline {paragraph}{UI}{64}{section*.61}%
\contentsline {paragraph}{URI}{64}{section*.62}%
\contentsline {paragraph}{USB OTG}{64}{section*.63}%
\contentsline {paragraph}{VM}{64}{section*.64}%
\contentsline {paragraph}{WSGI}{64}{section*.65}%
\contentsline {section}{\numberline {8}Bibliography}{65}{section.8}%
\contentsline {section}{\numberline {9}Appendices}{71}{section.9}%
\contentsline {subsection}{\numberline {9.1}Source Code}{71}{subsection.9.1}%
