class apiZones(Resource):
	def get(self):
		return db.getAllZones()

	def post(self):
		args = zoneArgsParser.parse_args()
		db.addZone(args.name, args.location, 
			args.description, args.is_inside)
		return {"Success":True}