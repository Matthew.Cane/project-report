FROM python:3
COPY requirements flask-api.py database.py dbInitScript.sql ./
RUN pip install --no-cache-dir -r ./requirements
CMD ["python", "flask-api.py"]
