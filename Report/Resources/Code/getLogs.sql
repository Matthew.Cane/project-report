SELECT
	Z_D.name AS 'Zone Name',
	User.first_name || ' ' || User.last_name AS 'User', 
	Event_Log.zone_authorised,
	Event_Log.key_authorised,
	datetime(time_stamp, 'unixepoch') AS 'Date/Time'
FROM Event_Log
LEFT JOIN User 
	ON User.user_id = Event_Log.user_id
LEFT JOIN (SELECT Door.door_id, Zone.name
	FROM Door 
	LEFT JOIN Zone 
		ON Zone.Zone_id = Door.zone_id) AS Z_D
	ON Z_D.door_id = Event_Log.door_id
ORDER BY Event_Log.time_stamp DESC