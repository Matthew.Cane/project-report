function addFormData(endpoint, formID) {
    var formData = {}
    const form = document.forms[formID].getElementsByTagName("input")
    for (var item of form) {
        var rowid = item.id
        var rowval = item.value
        formData[rowid] = rowval
    }
    doPost(endpoint, formData)
}